#[macro_use]
extern crate gfx;

pub mod camera;
pub mod primitives;
pub mod renderer;
pub mod scene;

pub use crate::camera::Camera;
pub use crate::primitives::Colour;
pub use crate::renderer::{Renderer, RenderingResult, SCREEN_HEIGHT, SCREEN_WIDTH};
pub use crate::scene::Scene;
