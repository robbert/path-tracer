use nalgebra::{Isometry3, Perspective3, Point3, Rotation3, Unit, Vector3};
use rand::Rng;
use std::f32::consts::FRAC_PI_2;

use crate::primitives::{Colour, Primitive, Ray};
use crate::renderer::{SCREEN_HEIGHT, SCREEN_WIDTH};

/// The minimum distance between two objects, used to prevent rays from intersecting with the object
/// that sent them out.
const EPSILON: f32 = 0.002;
const MAX_RECURSION: u32 = 10;

/// The scene object holds a list of renderable primitives and also contains information over the
/// camera and viewport. Rays can be cast into the scene with the `cast_ray()` method.
pub struct Scene {
    /// The projection matrix used to transform screen coordinates to world coordinates.
    pub projection: Perspective3<f32>,
    pub primitives: Vec<Box<Primitive + Sync>>,
}

impl Scene {
    pub fn new(primitives: Vec<Box<Primitive + Sync>>) -> Scene {
        Scene {
            projection: Perspective3::new(1.0, 1.2, 0.1, 100.0),
            primitives,
        }
    }

    /// Cast a single ray into the scene from a screen point. `x` and `y` should be in the range of
    /// `0..SCREEN_WIDTH - 1` and `0..SCREEN_HEIGHT - 1`. To prevent unneeded locking we don't store
    /// the `Camera` struct in the scene itself. Because of this `Camera::view_matrix` should be
    /// passed as the last arugment. For similar performance reasons we'll have to pass a RNG so we
    /// spend less time seeding RNGs.
    pub fn cast_ray<R: Rng>(
        &self,
        x: usize,
        y: usize,
        rng: &mut R,
        view_matrix: &Isometry3<f32>,
    ) -> Colour {
        let near_point = Point3::new(
            x as f32 / SCREEN_WIDTH as f32 * 2.0 - 1.0,
            y as f32 / SCREEN_HEIGHT as f32 * 2.0 - 1.0,
            -1.0,
        );
        let far_point = Point3::new(
            x as f32 / SCREEN_WIDTH as f32 * 2.0 - 1.0,
            y as f32 / SCREEN_HEIGHT as f32 * 2.0 - 1.0,
            1.0,
        );

        let near_view_point = view_matrix * self.projection.unproject_point(&near_point);
        let far_view_point = view_matrix * self.projection.unproject_point(&far_point);
        let ray = Ray::new(
            near_view_point,
            Unit::new_normalize(far_view_point - near_view_point),
        );

        self.trace_ray(&ray, 0, rng)
    }

    /// Traces a single ray through the scene. `depth` indicates the number of recursive cycles.
    fn trace_ray<R: Rng>(&self, ray: &Ray, depth: u32, rng: &mut R) -> Colour {
        if depth == MAX_RECURSION {
            return Colour::new(0.0, 0.0, 0.0);
        }

        // We only care about the nearest intersection
        // TODO: Use a proper data structure and lookup method
        let collision = self
            .primitives
            .iter()
            .filter_map(|primitive| {
                primitive
                    .intersects(ray)
                    .map(|intersection| (primitive, intersection))
            })
            .min_by(|&(_, t_l), &(_, t_r)| {
                t_l.partial_cmp(&t_r).expect("Invalid intersection: NaN")
            });

        if collision.is_none() {
            return Colour::new(0.0, 0.0, 0.0);
        }
        let (primitive, t_collision) = collision.unwrap();
        let intersection = ray.point_at(t_collision);

        // The next ray should be fired randomly into the intersection's normal's hemisphere
        let rotation = Rotation3::new(Vector3::new(
            rng.gen_range(-FRAC_PI_2, FRAC_PI_2),
            rng.gen_range(-FRAC_PI_2, FRAC_PI_2),
            rng.gen_range(-FRAC_PI_2, FRAC_PI_2),
        ));
        let normal = primitive.normal(&intersection);
        let next_ray_direction = Unit::new_normalize(rotation * normal.as_ref());
        let next_ray = Ray::new(
            intersection + next_ray_direction.as_ref() * EPSILON,
            next_ray_direction,
        );

        // We'll use Lambertian reflectance as our bidirectional reflectance distribution function
        // TODO: Don't hardcode the reflectance
        let brdf = 2.0 * 0.7 * next_ray.direction.dot(&normal);
        let emittance = primitive.colour();
        let reflected = self.trace_ray(&next_ray, depth + 1, rng);

        emittance + (brdf * reflected)
    }
}
