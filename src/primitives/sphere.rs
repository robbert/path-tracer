use nalgebra::{Point3, Unit, Vector3};

use super::{Colour, Primitive, Ray};

pub struct Sphere {
    pub colour: Colour,
    pub position: Point3<f32>,
    pub radius: f32,
}

impl Primitive for Sphere {
    fn colour(&self) -> Colour {
        self.colour
    }

    fn intersects(&self, ray: &Ray) -> Option<f32> {
        let center_vector = ray.origin - self.position;

        // We can leave out some values here because the ray's direction is always going to be
        // normalized
        let a = 1.0;
        let b = center_vector.dot(&(ray.direction.as_ref() * 2.0));
        let c = center_vector.dot(&center_vector) - self.radius * self.radius;

        let discriminant = (b * b - 4.0 * a * c).sqrt();
        if discriminant < 0.0 {
            return None;
        }

        // We need the lowest positive value for t, as we want to ignore collisions behind us
        let ts: [f32; 2] = [-b + discriminant, -b - discriminant];
        ts.iter()
            .filter(|&&t| t >= 0.0)
            .min_by(|t_1, t_2| t_1.partial_cmp(t_2).unwrap())
            .map(|t| t / 2.0)
    }

    fn normal(&self, point: &Point3<f32>) -> Unit<Vector3<f32>> {
        Unit::new_normalize(point - self.position)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn create_sphere() -> Sphere {
        Sphere {
            colour: Colour::zeros(),
            position: Point3::new(1.0, 0.0, 0.0),
            radius: 2.0,
        }
    }

    #[test]
    fn test_sphere_intersection() {
        let sphere = create_sphere();
        let ray = Ray::new(
            Point3::new(0.0, 0.0, 5.0),
            Unit::new_normalize(Vector3::new(0.0, 0.0, -1.0)),
        );
        let intersection = sphere.intersects(&ray).map(|t| ray.point_at(t));

        assert_eq!(Some(Point3::new(0.0, 0.0, 1.732_050_9)), intersection);
    }

    #[test]
    fn test_sphere_intersection_edge() {
        let sphere = create_sphere();
        let ray = Ray::new(
            Point3::new(-1.0, 0.0, 5.0),
            Unit::new_normalize(Vector3::new(0.0, 0.0, -1.0)),
        );
        let intersection = sphere.intersects(&ray).map(|t| ray.point_at(t));

        assert_eq!(Some(Point3::new(-1.0, 0.0, 0.0)), intersection);
    }

    #[test]
    /// When a ray is cast from within the sphere we should not be hitting anything behind us.
    fn test_sphere_intersection_inside() {
        let sphere = create_sphere();
        let ray = Ray::new(
            Point3::new(1.0, 0.0, 0.0),
            Unit::new_normalize(Vector3::new(0.0, 0.0, 1.0)),
        );
        let intersection = sphere.intersects(&ray).map(|t| ray.point_at(t));

        assert_eq!(Some(Point3::new(1.0, 0.0, 2.0)), intersection);
    }

    #[test]
    fn test_sphere_intersection_other_side() {
        let sphere = create_sphere();
        let ray = Ray::new(
            Point3::new(0.0, 0.0, -5.0),
            Unit::new_normalize(Vector3::new(0.0, 0.0, -1.0)),
        );
        let intersection = sphere.intersects(&ray).map(|t| ray.point_at(t));

        assert_eq!(None, intersection);
    }

    #[test]
    fn test_sphere_intersection_other_inside() {
        let sphere = create_sphere();
        let ray = Ray::new(
            Point3::new(1.0, 0.0, 0.0),
            Unit::new_normalize(Vector3::new(0.0, 0.0, -1.0)),
        );
        let intersection = sphere.intersects(&ray).map(|t| ray.point_at(t));

        assert_eq!(Some(Point3::new(1.0, 0.0, -2.0)), intersection);
    }

    #[test]
    fn test_sphere_intersection_straight() {
        let sphere = create_sphere();
        let ray = Ray::new(
            Point3::new(1.0, 0.0, 5.0),
            Unit::new_normalize(Vector3::new(0.0, 0.0, -1.0)),
        );
        let intersection = sphere.intersects(&ray).map(|t| ray.point_at(t));

        assert_eq!(Some(Point3::new(1.0, 0.0, 2.0)), intersection);
    }

    #[test]
    fn test_sphere_normal() {
        let sphere = create_sphere();
        let point = Point3::new(1.0, 0.0, 2.0);

        assert_eq!(
            Unit::new_normalize(Vector3::new(0.0, 0.0, 1.0)),
            sphere.normal(&point)
        );
    }
}
