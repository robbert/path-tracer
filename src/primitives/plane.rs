use nalgebra::{Point3, Unit, Vector3};

use super::{Colour, Primitive, Ray};

pub struct Plane {
    pub colour: Colour,

    /// Whether the plane should be span between four points or it should go on indefinitely. If the
    /// plane is infinitely large, at least the top left point should be set correctly.
    pub endless: bool,
    pub normal: Unit<Vector3<f32>>,
    pub bottom_left: Point3<f32>,
    pub bottom_right: Point3<f32>,
    pub top_left: Point3<f32>,
    pub top_right: Point3<f32>,
}

impl Plane {
    #[allow(dead_code)]
    pub fn new(
        colour: Colour,
        bottom_left: Point3<f32>,
        bottom_right: Point3<f32>,
        top_left: Point3<f32>,
        top_right: Point3<f32>,
    ) -> Plane {
        let normal = Unit::new_normalize((bottom_left - top_left).cross(&(top_right - top_left)));

        Plane {
            colour,
            endless: false,
            bottom_left,
            bottom_right,
            normal,
            top_left,
            top_right,
        }
    }

    #[allow(dead_code)]
    pub fn new_endless(colour: Colour, position: Point3<f32>, normal: Vector3<f32>) -> Plane {
        Plane {
            colour,
            endless: true,
            bottom_left: position,
            bottom_right: position,
            normal: Unit::new_normalize(normal),
            top_left: position,
            top_right: position,
        }
    }
}

impl Primitive for Plane {
    fn colour(&self) -> Colour {
        self.colour
    }

    fn intersects(&self, ray: &Ray) -> Option<f32> {
        // We'll solve the implicit equation Ax + By + Cz + D = 0
        let angle_cos = self.normal.dot(&ray.direction);
        if angle_cos == 0.0 {
            return None;
        }

        let t = self.normal.dot(&(self.top_left - ray.origin)) / angle_cos;
        if t < 0.0 {
            return None;
        }

        if self.endless {
            return Some(t);
        }

        // We'll have to ensure that the point is actually on the plane
        let intersection = ray.origin + ray.direction.as_ref() * t;

        let v1 = intersection - self.top_left;
        let v2 = self.top_right - self.top_left;
        let v3 = self.bottom_left - self.top_left;
        let d2 = v1.dot(&v2);
        let d3 = v1.dot(&v3);
        let l2 = v2.norm_squared();
        let l3 = v3.norm_squared();

        if 0.0 < d2 && d2 < l2 && 0.0 < d3 && d3 < l3 {
            Some(t)
        } else {
            None
        }
    }

    fn normal(&self, _point: &Point3<f32>) -> Unit<Vector3<f32>> {
        self.normal
    }
}

#[cfg(test)]
mod tests {
    use permutohedron;

    use super::*;

    fn create_plane() -> Plane {
        Plane::new(
            Colour::new(0.1, 0.7, 0.1),
            Point3::new(-100.0, -15.0, -100.0),
            Point3::new(100.0, -15.0, -100.0),
            Point3::new(100.0, -15.0, 100.0),
            Point3::new(-100.0, -15.0, 100.0),
        )
    }

    fn create_endless_plane() -> Plane {
        Plane::new_endless(
            Colour::new(0.1, 0.7, 0.1),
            Point3::new(0.0, -15.0, 0.0),
            Vector3::new(0.0, 1.0, 0.0),
        )
    }

    #[test]
    fn test_intersect() {
        let plane = create_plane();
        let ray = Ray::new(
            Point3::new(50.0, 0.0, 50.0),
            Unit::new_normalize(Vector3::new(0.0, -1.0, 0.0)),
        );
        let intersection = plane.intersects(&ray).map(|t| ray.point_at(t));

        assert_eq!(Some(Point3::new(50.0, -15.0, 50.0)), intersection);
    }

    #[test]
    fn test_intersect_angle() {
        let plane = create_plane();
        let ray = Ray::new(
            Point3::new(50.0, 0.0, 50.0),
            Unit::new_normalize(Vector3::new(-1.0, -1.0, -1.0)),
        );
        let intersection = plane.intersects(&ray).map(|t| ray.point_at(t));

        assert_eq!(Some(Point3::new(35.0, -15.0, 35.0)), intersection);
    }

    #[test]
    /// There should not be intersections behind the origin of the ray.
    fn test_intersect_behind() {
        let plane = create_plane();
        let ray = Ray::new(
            Point3::new(0.0, 0.0, 0.0),
            Unit::new_normalize(Vector3::new(0.0, 1.0, 0.0)),
        );

        assert_eq!(None, plane.intersects(&ray));
    }

    #[test]
    fn test_intersect_miss() {
        let plane = create_plane();
        let ray = Ray::new(
            Point3::new(200.0, 0.0, 0.0),
            Unit::new_normalize(Vector3::new(0.0, -1.0, 0.0)),
        );

        assert_eq!(None, plane.intersects(&ray));
    }

    #[test]
    /// The order of the points should not matter, or else we might have weird issues with back
    /// faces.
    fn test_intersect_order() {
        let ray = Ray::new(
            Point3::new(50.0, 0.0, 50.0),
            Unit::new_normalize(Vector3::new(0.0, -1.0, 0.0)),
        );

        let mut points = [
            Point3::new(-100.0, -15.0, -100.0),
            Point3::new(100.0, -15.0, -100.0),
            Point3::new(100.0, -15.0, 100.0),
            Point3::new(-100.0, -15.0, 100.0),
        ];
        permutohedron::heap_recursive(&mut points, |permutation| {
            let plane = Plane::new(
                Colour::new(0.1, 0.7, 0.1),
                permutation[0],
                permutation[1],
                permutation[2],
                permutation[3],
            );
            let intersection = plane.intersects(&ray).map(|t| ray.point_at(t));

            assert_eq!(Some(Point3::new(50.0, -15.0, 50.0)), intersection)
        });
    }

    #[test]
    /// The order of the points should not matter, or else we might have weird issues with back
    /// faces.
    fn test_intersect_order_miss() {
        let ray = Ray::new(
            Point3::new(200.0, 0.0, 0.0),
            Unit::new_normalize(Vector3::new(0.0, -1.0, 0.0)),
        );

        let mut points = [
            Point3::new(-100.0, -15.0, -100.0),
            Point3::new(100.0, -15.0, -100.0),
            Point3::new(100.0, -15.0, 100.0),
            Point3::new(-100.0, -15.0, 100.0),
        ];
        permutohedron::heap_recursive(&mut points, |permutation| {
            let plane = Plane::new(
                Colour::new(0.1, 0.7, 0.1),
                permutation[0],
                permutation[1],
                permutation[2],
                permutation[3],
            );

            assert_eq!(None, plane.intersects(&ray));
        });
    }

    #[test]
    /// There should not be an intersection if the plane and the ray are parallel.
    fn test_intersect_parallel() {
        let plane = create_plane();
        let ray = Ray::new(
            Point3::new(0.0, 0.0, 0.0),
            Unit::new_normalize(Vector3::new(1.0, 0.0, 1.0)),
        );

        assert_eq!(None, plane.intersects(&ray));
    }

    // The following tests are exactle the same as the above, but for endless planes. Only the
    // `test_intersect_miss_endless` should yield different results.

    #[test]
    fn test_intersect_endless() {
        let plane = create_endless_plane();
        let ray = Ray::new(
            Point3::new(50.0, 0.0, 50.0),
            Unit::new_normalize(Vector3::new(0.0, -1.0, 0.0)),
        );
        let intersection = plane.intersects(&ray).map(|t| ray.point_at(t));

        assert_eq!(Some(Point3::new(50.0, -15.0, 50.0)), intersection);
    }

    #[test]
    fn test_intersect_angle_endless() {
        let plane = create_endless_plane();
        let ray = Ray::new(
            Point3::new(50.0, 0.0, 50.0),
            Unit::new_normalize(Vector3::new(-1.0, -1.0, -1.0)),
        );
        let intersection = plane.intersects(&ray).map(|t| ray.point_at(t));

        assert_eq!(Some(Point3::new(35.0, -15.0, 35.0)), intersection);
    }

    #[test]
    /// There should not be intersections behind the origin of the ray.
    fn test_intersect_behind_endless() {
        let plane = create_endless_plane();
        let ray = Ray::new(
            Point3::new(0.0, 0.0, 0.0),
            Unit::new_normalize(Vector3::new(0.0, 1.0, 0.0)),
        );

        assert_eq!(None, plane.intersects(&ray));
    }

    #[test]
    fn test_intersect_miss_endless() {
        let plane = create_endless_plane();
        let ray = Ray::new(
            Point3::new(200.0, 0.0, 0.0),
            Unit::new_normalize(Vector3::new(0.0, -1.0, 0.0)),
        );
        let intersection = plane.intersects(&ray).map(|t| ray.point_at(t));

        assert_eq!(Some(Point3::new(200.0, -15.0, 0.0)), intersection);
    }

    #[test]
    /// There should not be an intersection if the plane and the ray are parallel.
    fn test_intersect_parallel_endless() {
        let plane = create_endless_plane();
        let ray = Ray::new(
            Point3::new(0.0, 0.0, 0.0),
            Unit::new_normalize(Vector3::new(1.0, 0.0, 1.0)),
        );

        assert_eq!(None, plane.intersects(&ray));
    }

    #[test]
    fn test_normal() {
        let plane = create_plane();
        let normal = plane.normal(&plane.top_left);

        assert_eq!(Unit::new_normalize(Vector3::new(0.0, 1.0, 0.0)), normal);
    }
}
