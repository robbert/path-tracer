//! This module contains a trait describing primitives, and several primitives that can be added to
//! a scene.

use nalgebra::{Point3, Unit, Vector3};

pub use self::plane::Plane;
pub use self::sphere::Sphere;

mod plane;
mod sphere;

pub type Colour = Vector3<f32>;

pub trait Primitive {
    fn colour(&self) -> Colour;
    /// Calculates an intersection between a ray and the object, returning the value of `t` for
    /// which `ray.origin + ray.direction * t` hits the object.
    fn intersects(&self, ray: &Ray) -> Option<f32>;
    /// Calculates the normal at an intersection point.
    fn normal(&self, point: &Point3<f32>) -> Unit<Vector3<f32>>;
}

pub struct Ray {
    pub origin: Point3<f32>,
    /// The direction the ray is heading in.
    pub direction: Unit<Vector3<f32>>,
}

impl Ray {
    pub fn new(origin: Point3<f32>, direction: Unit<Vector3<f32>>) -> Ray {
        Ray { origin, direction }
    }

    /// Calculates a poitn along the ray for a given value of `t`.
    pub fn point_at(&self, t: f32) -> Point3<f32> {
        self.origin + self.direction.as_ref() * t
    }
}
