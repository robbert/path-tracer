use gfx;
use gfx::format::{ChannelType, Rgba32F, Swizzle, R32_G32_B32_A32};
use gfx::handle::{
    Buffer, DepthStencilView, RenderTargetView, Sampler, ShaderResourceView, Texture,
};
use gfx::memory::{cast_slice, Bind, Usage};
use gfx::texture::{FilterMethod, SamplerInfo, WrapMode};
use gfx::traits::FactoryExt;
use gfx::{Encoder, Factory, PipelineState, Slice};
use gfx_device_gl::{CommandBuffer, Device, Resources};
use gfx_glyph::{GlyphBrush, GlyphBrushBuilder, Scale, Section};
use glutin::dpi::LogicalSize;
use glutin::ElementState::{Pressed, Released};
use glutin::{
    Api, EventsLoop, GlRequest, GlWindow, KeyboardInput, MouseButton, VirtualKeyCode, WindowEvent,
};
use nalgebra::Vector3;
use std::collections::HashSet;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, RwLock};

use crate::camera::Camera;
use crate::primitives::Colour;

const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
pub const SCREEN_WIDTH: usize = 512;
pub const SCREEN_HEIGHT: usize = 512;

/// The camera's movement speed in units per second when holding a movement button.
pub const MOVEMENT_SPEED: f32 = 2.0;

pub type ColorFormat = gfx::format::Rgba8;
pub type DepthFormat = gfx::format::DepthStencil;

/// The results of the path tracer are stored in here. Texture is a giant row-by-row array
/// containing the total, summed colour values for each pixel. These have to be averaged by dividing
/// by `iterations` in order get a usable result. This division is currently done in the fragment
/// shader.
pub struct RenderingResult {
    pub iterations: u32,
    pub texture: Vec<Colour>,

    /// Whether the scene should redraw. This is done using atomics to avoid the need for multiple
    /// write locks.
    pub should_clear: AtomicBool,
}

impl RenderingResult {
    pub fn empty() -> RenderingResult {
        RenderingResult {
            iterations: 0,
            texture: vec![Colour::zeros(); SCREEN_WIDTH * SCREEN_HEIGHT],
            should_clear: AtomicBool::new(false),
        }
    }

    pub fn clear(&mut self) {
        self.iterations = 0;
        self.texture = vec![Colour::zeros(); SCREEN_WIDTH * SCREEN_HEIGHT];
    }
}

/// The renderer handles the actual drawing of the path traced result but also the creation of
/// windows. As the event loop is tied to the window creation, camera manipulation is also done in
/// the `run()` method.
pub struct Renderer {
    rendering_result: Arc<RwLock<RenderingResult>>,
    camera: Arc<RwLock<Camera>>,

    // Internal buffers and pipelines for rendering
    colour_view: RenderTargetView<Resources, ColorFormat>,
    depth_view: DepthStencilView<Resources, DepthFormat>,
    events_loop: EventsLoop,
    device: Device,
    encoder: Encoder<Resources, CommandBuffer>,
    globals_buffer: Buffer<Resources, Globals>,
    shaders: PipelineState<Resources, pipe::Meta>,
    slice: Slice<Resources>,
    text_renderer: GlyphBrush<'static, Resources, gfx_device_gl::Factory>,
    texture: Texture<Resources, R32_G32_B32_A32>,
    texture_sampler: Sampler<Resources>,
    texture_view: ShaderResourceView<Resources, [f32; 4]>,
    vertex_buffer: Buffer<Resources, Vertex>,
    window: GlWindow,
}

gfx_defines! {
    vertex Vertex {
        pos: [f32; 2] = "v_pos",
    }

    constant Globals {
        u_iterations: u32 = "u_iterations",
    }

    pipeline pipe {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        tex: gfx::TextureSampler<[f32; 4]> = "u_texture",
        iterations: gfx::ConstantBuffer<Globals> = "Globals",
        out: gfx::RenderTarget<ColorFormat> = "colour",
    }
}

impl Renderer {
    /// Initializes the rendering context and opens a window for rendering.
    pub fn init(
        rendering_result: Arc<RwLock<RenderingResult>>,
        camera: Arc<RwLock<Camera>>,
    ) -> Renderer {
        let events_loop = glutin::EventsLoop::new();
        let window_builder = glutin::WindowBuilder::new()
            .with_title("Path Tracer")
            .with_dimensions(LogicalSize::new(SCREEN_WIDTH as f64, SCREEN_HEIGHT as f64));
        let context_builder = glutin::ContextBuilder::new()
            .with_gl(GlRequest::Specific(Api::OpenGl, (3, 3)))
            .with_vsync(true);
        let (window, device, mut factory, colour_view, depth_view) =
            gfx_window_glutin::init::<ColorFormat, DepthFormat>(
                window_builder,
                context_builder,
                &events_loop,
            )
            .expect("Could not create window");

        let shaders = factory
            .create_pipeline_simple(
                include_bytes!("shaders/vs.glsl"),
                include_bytes!("shaders/fs.glsl"),
                pipe::new(),
            )
            .expect("Could not create graphics pipeline");
        // TODO: Try different settings for these things
        let texture = factory
            .create_texture::<R32_G32_B32_A32>(
                gfx::texture::Kind::D2(
                    SCREEN_WIDTH as u16,
                    SCREEN_HEIGHT as u16,
                    gfx::texture::AaMode::Single,
                ),
                1,
                Bind::all(),
                Usage::Dynamic,
                Some(ChannelType::Float),
            )
            .expect("Couldn't create texture on GPU");
        let texture_view = factory
            .view_texture_as_shader_resource::<Rgba32F>(&texture, (0, 1), Swizzle::new())
            .expect("Couldn't create view for texture on GPU");
        let texture_sampler = factory.create_sampler(SamplerInfo::new(
            FilterMethod::Anisotropic(16),
            WrapMode::Mirror,
        ));

        let globals_buffer = factory.create_constant_buffer(1);

        const QUAD: [Vertex; 6] = [
            Vertex { pos: [-1.0, -1.0] },
            Vertex { pos: [1.0, -1.0] },
            Vertex { pos: [-1.0, 1.0] },
            Vertex { pos: [1.0, -1.0] },
            Vertex { pos: [-1.0, 1.0] },
            Vertex { pos: [1.0, 1.0] },
        ];
        let (vertex_buffer, slice) = factory.create_vertex_buffer_with_slice(&QUAD, ());
        let encoder: Encoder<_, _> = factory.create_command_buffer().into();

        let text_renderer =
            GlyphBrushBuilder::using_font_bytes(include_bytes!("fonts/DejaVuSans.ttf") as &[u8])
                .build(factory);

        Renderer {
            rendering_result,
            camera,

            colour_view,
            depth_view,
            events_loop,
            device,
            encoder,
            globals_buffer,
            shaders,
            slice,
            text_renderer,
            texture,
            texture_sampler,
            texture_view,
            vertex_buffer,
            window,
        }
    }

    /// Starts drawing and accepting events. This blocks the thread.
    pub fn run(&mut self) {
        let mut running = true;
        let mut allow_movement = false;
        let mut should_resize = false;
        let mut pressed_keys = HashSet::new();
        let mut previous_time = time::now();
        while running {
            let current_time = time::now();
            let elapsed_time = current_time - previous_time;
            previous_time = current_time;

            self.events_loop.poll_events(|event| {
                if let glutin::Event::WindowEvent { event, .. } = event {
                    match event {
                        WindowEvent::CloseRequested => running = false,
                        WindowEvent::Resized { .. } => should_resize = true,
                        WindowEvent::MouseInput {
                            state,
                            button: MouseButton::Right,
                            ..
                        } => {
                            allow_movement = state == Pressed;
                        }
                        WindowEvent::KeyboardInput {
                            input:
                                KeyboardInput {
                                    state,
                                    virtual_keycode: Some(key),
                                    ..
                                },
                            ..
                        } => {
                            match state {
                                Pressed => pressed_keys.insert(key),
                                Released => pressed_keys.remove(&key),
                            };
                        }
                        _ => {}
                    }
                }
            });

            // TODO: Throttle this or otherwise prevent drawing from blocking the path tracing
            self.draw();

            // We'll have to manually resize the views, or else parts of the window will be blank or
            // not rendered correctly
            if should_resize {
                gfx_window_glutin::update_views(
                    &self.window,
                    &mut self.colour_view,
                    &mut self.depth_view,
                );
                should_resize = false;
            }

            if pressed_keys.contains(&VirtualKeyCode::Escape)
                || pressed_keys.contains(&VirtualKeyCode::Q)
            {
                running = false;
            }

            if !allow_movement {
                continue;
            }

            let movement = MOVEMENT_SPEED * (elapsed_time.num_milliseconds() as f32 / 1000.0);
            let mut movement_delta = Vector3::zeros();
            if pressed_keys.contains(&VirtualKeyCode::W) {
                movement_delta.z -= movement;
            }
            if pressed_keys.contains(&VirtualKeyCode::S) {
                movement_delta.z += movement;
            }
            if pressed_keys.contains(&VirtualKeyCode::A) {
                movement_delta.x -= movement;
            }
            if pressed_keys.contains(&VirtualKeyCode::D) {
                movement_delta.x += movement;
            }
            if pressed_keys.contains(&VirtualKeyCode::LControl) {
                movement_delta.y -= movement;
            }
            if pressed_keys.contains(&VirtualKeyCode::Space) {
                movement_delta.y += movement;
            }

            if movement_delta.norm() > 0.0 {
                self.camera
                    .write()
                    .unwrap()
                    .update_relative_position(movement_delta);
                self.rendering_result
                    .read()
                    .unwrap()
                    .should_clear
                    .store(true, Ordering::Relaxed);
            }
        }
    }

    /// Draws a single frame.
    fn draw(&mut self) {
        let (iterations, texture_data) = {
            let result = self.rendering_result.read().unwrap();
            let iterations = result.iterations;
            let texture_data: Vec<[u32; 4]> = self
                .rendering_result
                .read()
                .unwrap()
                .texture
                .iter()
                .map(|colour| {
                    // glx won't let us upload f32s for some reason, so we'll have to use dark magic
                    // and reinterpret them as u32s
                    let casted: &[u32] = cast_slice(colour.as_slice());
                    [casted[0], casted[1], casted[2], 0]
                })
                .collect();

            (iterations, texture_data)
        };

        self.encoder.update_constant_buffer(
            &self.globals_buffer,
            &Globals {
                u_iterations: iterations,
            },
        );
        self.encoder
            .update_texture::<R32_G32_B32_A32, Rgba32F>(
                &self.texture,
                None,
                self.texture.get_info().to_image_info(0),
                texture_data.as_slice(),
            )
            .expect("Couldn't write to texture");
        self.text_renderer.queue(Section {
            text: &format!("Iterations: {}", iterations),
            screen_position: (12.0, 8.0),
            color: [1.0; 4],
            scale: Scale::uniform(20.0),
            ..Section::default()
        });

        self.encoder.clear(&self.colour_view, BLACK);
        self.encoder.draw(
            &self.slice,
            &self.shaders,
            &pipe::Data {
                vbuf: self.vertex_buffer.clone(),
                tex: (self.texture_view.clone(), self.texture_sampler.clone()),
                iterations: self.globals_buffer.clone(),
                out: self.colour_view.clone(),
            },
        );
        self.text_renderer
            .draw_queued(&mut self.encoder, &self.colour_view, &self.depth_view)
            .expect("Couldn't draw text");

        self.encoder.flush(&mut self.device);
        self.window.swap_buffers().unwrap();
    }
}
