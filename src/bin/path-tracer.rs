use nalgebra::{Point3, Vector3};
use rand::{rngs::SmallRng, thread_rng, SeedableRng};
use rayon::prelude::*;
use std::sync::atomic::Ordering;
use std::sync::{Arc, RwLock};

use path_tracer::primitives::{Plane, Sphere};
use path_tracer::{Camera, Colour, Renderer, RenderingResult, Scene, SCREEN_HEIGHT, SCREEN_WIDTH};

fn main() {
    let camera = Arc::new(RwLock::new(Camera::new()));
    let result = Arc::new(RwLock::new(RenderingResult::empty()));
    {
        let camera = Arc::clone(&camera);
        let result = Arc::clone(&result);
        rayon::spawn(move || {
            let scene = Scene::new(vec![
                Box::new(Plane::new_endless(
                    Colour::zeros(),
                    Point3::new(0.0, -3.0, 0.0),
                    Vector3::new(0.0, 1.0, 0.0),
                )),
                Box::new(Plane::new_endless(
                    Colour::zeros(),
                    Point3::new(0.0, 10.0, 0.0),
                    Vector3::new(0.0, -1.0, 0.0),
                )),
                Box::new(Sphere {
                    colour: Colour::new(1.0, 0.0, 0.0),
                    position: Point3::new(2.0, 2.0, -14.0),
                    radius: 5.0,
                }),
                Box::new(Sphere {
                    colour: Colour::new(0.0, 1.0, 0.0),
                    position: Point3::new(6.0, 2.0, -9.0),
                    radius: 1.5,
                }),
                Box::new(Sphere {
                    colour: Colour::new(0.0, 0.0, 1.0),
                    position: Point3::new(4.5, 1.0, -9.5),
                    radius: 0.3,
                }),
                Box::new(Sphere {
                    colour: Colour::new(0.5, 0.5, 0.5),
                    position: Point3::new(12.0, -2.4, -20.0),
                    radius: 0.5,
                }),
            ]);

            // Instead of reseeding a PRNG for every row in every iteration we'll simply reuse a
            // pool of cheap PRNGS. Ideally we would reseed these every X iterations.
            let mut thread_rng = thread_rng();
            let mut rngs: Vec<_> = (0..SCREEN_HEIGHT)
                .map(|_| SmallRng::from_rng(&mut thread_rng).unwrap())
                .collect();

            loop {
                // We'll sum aquired color values for every pixel so we can average them during
                // rendering
                let view_matrix = camera.read().unwrap().view_matrix;
                let mut result = result.write().unwrap();

                if result
                    .should_clear
                    .compare_and_swap(true, false, Ordering::Relaxed)
                {
                    result.clear();
                }

                result.iterations += 1;
                result
                    .texture
                    .par_chunks_mut(SCREEN_WIDTH as usize)
                    .enumerate()
                    .zip_eq(&mut rngs)
                    .for_each(|((y, row), rng)| {
                        for (x, pixel) in row.iter_mut().enumerate() {
                            *pixel += scene.cast_ray(x, y, rng, &view_matrix);
                        }
                    });
            }
        });
    }

    let mut renderer = Renderer::init(result, camera);
    renderer.run();
}
