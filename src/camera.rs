use nalgebra::{Isometry3, Translation, UnitQuaternion, Vector3};
use std::f32::consts::PI;

pub struct Camera {
    pub view_matrix: Isometry3<f32>,
}

impl Camera {
    pub fn new() -> Camera {
        let rotation = UnitQuaternion::from_euler_angles(0.1 * PI, -0.1 * PI, 0.0);
        let translation = Translation::from(Vector3::new(1.0, -1.6, -4.8));

        Camera {
            view_matrix: Isometry3::from_parts(translation, rotation),
        }
    }

    pub fn update_direction(&mut self, delta_x: f32, delta_y: f32) {
        let rotation = UnitQuaternion::from_euler_angles(delta_y, delta_x, 0.0);
        self.view_matrix.append_rotation_mut(&rotation);
    }

    pub fn update_position(&mut self, delta: Vector3<f32>) {
        let translation = Translation::from(delta);
        self.view_matrix.append_translation_mut(&translation);
    }

    /// Adds to to camera's translation, taking into account the viewing direction. This only takes
    /// rotation along the Y-axis into account.
    pub fn update_relative_position(&mut self, delta: Vector3<f32>) {
        let mut rotated = self.view_matrix * delta;
        rotated.y = delta.y;

        let translation = Translation::from(rotated);
        self.view_matrix.append_translation_mut(&translation);
    }
}

impl Default for Camera {
    fn default() -> Self {
        Self::new()
    }
}
